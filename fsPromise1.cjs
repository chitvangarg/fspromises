const fs = require("fs/promises")
const path = require("path")

function createRandomJsonFiles(directoryPath, numberOfFiles) {
    // Directory creation using fs module

    return new Promise((resolve, reject) => {
        fs.mkdir(directoryPath).then(() => {
            let createdFileCount = 0
                const filePaths = []

                // pushing file data
                for (let file = 1; file <= numberOfFiles; file++) {
                    const randomData = { dummyKey: Math.random() }

                    // Creating path
                    const filePath = path.join(
                        directoryPath,
                        `file${file}.json`
                    )
                    const fileContent = JSON.stringify(randomData)

                    // Asynchronously writing files
                    fs.writeFile(filePath, fileContent).then(() => {
                        filePaths.push(filePath)
                        createdFileCount++

                        if (createdFileCount === numberOfFiles) {
                            resolve(filePaths)
                        }
                    }).catch((err) => {
                        reject(err)
                    })
                }
            }).catch((err) => {
                reject(err)
            })
                
        })
}

function deleteFilesSimultaneously(filePaths) {
    return new Promise((resolve, reject) => {
        let filesDeleted = []

        // unlinks or permanent delete files in Randomjson
        function deleteNextFiles(filepath, index) {
            // base condition
            if (index === filepath.length) {
                resolve(filesDeleted)
            }

            // Pushing fs unlink promises to unlinkfilepromises
            fs.unlink(filepath[index]).then(() => {
                filesDeleted.push(filepath[index])

                deleteNextFiles(filePaths, index+1)
            }).catch((err) => {
                reject(err)                
            })

        }

        deleteNextFiles(filePaths, 0)
    })
}

function randomJSONfiles(directoryPath, numberOfFiles){
    createRandomJsonFiles(directoryPath, numberOfFiles)
        .then((res) => {
            console.log(res)
            return deleteFilesSimultaneously(res)
        })
        .then((delRes) => {
            console.log(delRes)
        })
        .catch((err) => {
            console.log(err.message)
        })
}

module.exports = randomJSONfiles


