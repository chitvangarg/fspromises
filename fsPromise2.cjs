const fs = require("fs/promises")

// Step 1: Read the given file lipsum.txt
function readOriginalFile(lipsumFilePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(lipsumFilePath, "utf-8")
            .then((data) => {
                resolve(data)
            })
            .catch((err) => {
                reject(err)
            })
    })
}

// Step 2: Convert the content to uppercase & write to a new file.
function convertToUppercaseAndWrite(data) {
    return new Promise((resolve, reject) => {
        const uppercaseData = data.toUpperCase()
        const newFilename = "uppercase.txt"

        fs.writeFile(newFilename, uppercaseData, "utf8")
            .then(() => {
                appendFilenames(newFilename)
                    .then((value) => {
                        resolve(value)
                    })
                    .catch((err) => {
                        console.log(err.message)
                    })
            })
            .catch((err) => {
                reject(err)
            })
    })
}

// Step 3: Read the new file, convert it to lower case, split into sentences, and write to a new file.
function convertToLowercaseAndSplit(filePath) {
    return new Promise((resolve, reject) => {
        fs.readFile(filePath, "utf-8")
            .then((data) => {
                const lowercaseData = data.toLowerCase()
                const sentences = lowercaseData.split(/[.!?]/).filter(Boolean)
                const newFilename = "sentences.txt"

                fs.writeFile(newFilename, sentences.join("\n"), "utf8")
                    .then(() => {
                        appendFilenames(newFilename)
                            .then((value) => {
                                resolve(value)
                            })
                            .catch((err) => {
                                reject(err.message)
                            })
                    })
                    .catch((err) => {
                        reject(err)
                    })
            })
            .catch((err) => {
                reject(err)
            })
    })
}

// Step 4: Read the new files, sort the content, and write it to a new file.
function sortContentAndWrite(files) {
    return new Promise((resolve, reject) => {
        const readFilesCount = files.length
        let completedReads = 0
        let fileContents = []

        for (let file = 0; file < readFilesCount; file++) {
            fs.readFile(files[file], "utf8")
                .then((data) => {
                    fileContents.push(data)
                    completedReads++

                    if (completedReads === readFilesCount) {
                        const sortedContent = fileContents
                            .join("\n")
                            .split("\n")
                            .sort()
                            .join("\n")
                        const newFilename = "sorted.txt"

                        fs.writeFile(newFilename, sortedContent, "utf8")
                            .then(() => {
                                appendFilenames(newFilename)
                                    .then((value) => {
                                        resolve(value)
                                    })
                                    .catch((err) => {
                                        reject(err.message)
                                    })
                            })
                            .catch((err) => {
                                if (err) {
                                    reject(err)
                                } else {
                                }
                            })
                    }
                })
                .catch((err) => {
                    reject(err)
                })
        }
    })
}

// Step 5: Read the contents of filenames.txt and delete all the new files mentioned in that list simultaneously.
function readFilenamesAndDeleteFiles() {
    return new Promise((resolve, reject) => {
        fs.readFile("filenames.txt", "utf8")
            .then((data) => {
                const filenames = data.split("\n").filter(Boolean)
                let completedDeletes = 0
                const totalDeletes = filenames.length

                filenames.forEach((filename) => {
                    fs.unlink(filename)
                        .then((data) => {
                            completedDeletes++

                            if (completedDeletes === totalDeletes) {
                                resolve("All files deleted successfully.")
                            }
                        })
                        .catch((err) => {
                            reject(err)
                        })
                })
            })
            .catch((err) => {
                reject(err)
            })
    })
}

function appendFilenames(filename) {
    return new Promise((resolve, reject) => {
        fs.appendFile("filenames.txt", `${filename}\n`, "utf-8")
            .then(() => {
                resolve(filename)
            })
            .catch((err) => {
                reject(err)
            })
    })
}
// Execute the steps in sequence using callbacks.

const fsPromise2 = function () {
    const files = []

    readOriginalFile("./lorem.txt")
        .then((data) => {
            return convertToUppercaseAndWrite(data)
        })
        .then((result) => {
            files.push(result)
            return convertToLowercaseAndSplit(result)
        })
        .then((result) => {
            files.push(result)
            return sortContentAndWrite(files)
        })
        .then(() => {
            readFilenamesAndDeleteFiles().then((res) => {
                console.log(res)
            })
        })
}

module.exports = fsPromise2